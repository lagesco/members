### Sobre o projeto
Expor um serviço de Sócio Torcedor seguindo a especificação.

## Premissas
Eu, como usuário, quero me cadastrar através de uma API que me permite participar de algumas
campanhas. Para tanto, os critérios de aceite dessa história são:

Dado um E-mail que já existe, informar que o cadastro já foi efetuado, porém, caso o cliente
não tenha nenhuma campanha associada, o serviço deverá enviar as novas campanhas
como resposta;

O Cadastro deve ser composto de:

- Nome Completo;
- E-mail;
- Data de Nascimento;
- Meu Time do Coração;

O Cliente não pode ter mais de um cadastro ativo;

Ao efetuar o cadastro do usuário, utilize os serviços criados anteriormente para efetuar as
operações e as associações necessárias:

- O Cadastramento do cliente ocorre antes da associação com as campanhas, ou seja,
- processo de cadastro e associação ocorre em dois momentos separados;
- O Usuários pode ter N Campanhas associadas a ele; Lembrando que as campanhas
são associadas ao Time do Coração;
- A associação do usuário as campanhas podem ocorrer em dois momentos:

> Se for usuário novo: Após o cadastramento do usuário, o sistema deverá
solicitar as campanhas ativas para aquele time do coração e efetuar a
associação;

> Se for um usuário já cadastrado: Deverá ser feita a associação das
campanhas novas, ou seja, as que o usuário daquele time do coração não
tem relacionamento até o momento.

O Consumo das listas das campanhas deve ser feita via Serviço de **campaigns** [https://bitbucket.org/lagesco/campaigns](https://bitbucket.org/lagesco/campaigns);

O Cadastramento das campanhas deverá ser feito via Serviço (API, conforme descrito no exercício anterior) (???)

O Cadastramento não pode ser influenciado pelo serviço das campanhas, caso o serviço das campanhas não esteja "UP";

## Resolução

Abaixo segue informações relativas a resolução do problema:

### Informações da aplicação
Porta: **8091**

Endpoint para as campanhas: **/api/v1/members**

Documentação e teste: **/swagger-ui.html** (ex: http://localhost:8090/swagger-ui.html)

> Para documentar a API usei o Swagger, caso necessite mais informações: [swagger.io](http://swagger.io)

Para realizar a persistencia foi utilizado o **MongoDB**.

_Por questões de mémoria na maquina dev esta aplicação utiliza o mesmo banco que a campaigns_ 

### Dependências

Para compilar esta aplicação é necessário que tenha instalado o **campaigns-common**, disponível em: [https://bitbucket.org/lagesco/campaigns-common](https://bitbucket.org/lagesco/campaigns-common)

Para realizar a instalação no repositório local, abra o terminal, navegue até o diretório da aplicação (_campaigns-common_) e execute o comando abaixo:

```cmd
mvn clean install
```

Lembre-se de manter o docker rodando para utilizar esta aplicação, e para isso execute o `start.sh` em **campaigns**, displonível: [https://bitbucket.org/lagesco/campaigns](https://bitbucket.org/lagesco/campaigns).

### Executando os teste
Para executar os teste, abra o terminal, navegue até o diretório da aplicação e execute o comando abaixo:

```cmd
mvn test
```

### Executando a aplicação

Esta aplicação utiliza o docker e docker-compose, caso não o tenha acesse [aqui](https://docs.docker.com/install/).
Para executar a aplicação, abra o terminal, navegue até o diretório da aplicação e execute o comando abaixo:

```cmd
./start.sh
```

Depois de iniciado o server poderá acessa-lo em:
[http://localhost:8091/swagger-ui.html](http://localhost:8091/swagger-ui.html)

### Exemplos de utilização

- Para cadastras um membro:

Execute um `POST` em **http://localhost:8091/api/v1/members/**.
Informando o `body`:
```json
{
  "name": "Hugo Broks",
  "birthDate": "2007-03-03",
  "email": "hugo2@abc.com",
  "teamId": "TEAM-2"
}
```
> Obs.: Caso a base não tenha sido cadastrado novas campanhas para a data atual, nada será retornado. 

### Tecnologias utilizadas

- Java 8
- Spring boot
- MongoDB
- Swagger
- JUnit/AssertJ/Mockito
- Spring Data MongoDB
- Spring Web MVC
- Spring Cloud Feign
- Spring Cloud Hystrix
- Kafka
- Spring Kafka