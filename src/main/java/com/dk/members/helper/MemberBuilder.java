package com.dk.members.helper;

import com.dk.members.model.Member;

import java.time.LocalDate;

/**
 * @author Caio Andrade (dk)
 */
public final class MemberBuilder {
    private String id;
    private String name;
    private String email;
    private LocalDate birthDate;
    private String teamId;

    private MemberBuilder() {
    }

    public static MemberBuilder create() {
        return new MemberBuilder();
    }

    public MemberBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public MemberBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public MemberBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public MemberBuilder withBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public MemberBuilder withTeamId(String teamId) {
        this.teamId = teamId;
        return this;
    }

    public Member build() {
        Member member = new Member();
        member.setId(id);
        member.setName(name);
        member.setEmail(email);
        member.setBirthDate(birthDate);
        member.setTeamId(teamId);
        return member;
    }
}
