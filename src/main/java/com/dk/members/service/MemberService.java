package com.dk.members.service;

import com.dk.campaigns.domain.CampaignResource;
import com.dk.members.model.Member;
import com.dk.members.model.MemberCampaign;

import java.util.List;

/**
 * Interface para serviço o Membro
 *
 * @author Caio Andrade (dk)
 */
public interface MemberService {

    Member save(Member member);

    void delete(String id);

    Member findOne(String id);

    Member findByEmail(String email);

    List<Member> findAll();

    List<MemberCampaign> findByMemberAndCampaignIn(Member member, List<CampaignResource> campaigns);

    void saveCampaigns(Member member, List<CampaignResource> campaigns);

}
