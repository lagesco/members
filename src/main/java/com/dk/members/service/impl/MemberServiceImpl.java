package com.dk.members.service.impl;

import com.dk.campaigns.domain.CampaignResource;
import com.dk.members.model.Member;
import com.dk.members.model.MemberCampaign;
import com.dk.members.repository.MemberCampaignRepository;
import com.dk.members.repository.MemberRepository;
import com.dk.members.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Implementação do Serviço de Membro
 *
 * @author Caio Andrade (dk)
 */
@Service
public class MemberServiceImpl implements MemberService {
    @Autowired
    private MemberRepository repository;

    @Autowired
    private MemberCampaignRepository memberCampaignRepository;


    @Override
    public Member save(Member member) {
        return repository.save(member);
    }

    @Override
    public void delete(String id) {
        repository.delete(id);
    }

    @Override
    public Member findOne(String id) {
        return repository.findOne(id);
    }

    @Override
    public Member findByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Override
    public List<Member> findAll() {
        return repository.findAll();
    }

    @Override
    public List<MemberCampaign> findByMemberAndCampaignIn(Member member, List<CampaignResource> campaigns) {
        return memberCampaignRepository.findByMemberAndCampaignIdIn(member,
                campaigns.stream().map(CampaignResource::getId).collect(Collectors.toSet()));
    }

    @Override
    public void saveCampaigns(Member member, List<CampaignResource> campaigns) {
        Stream<CampaignResource> stream;

        if (CollectionUtils.isEmpty(campaigns)) {
            stream = campaigns.stream();
        } else {
            List<MemberCampaign> existingCampaigns = this.findByMemberAndCampaignIn(member, campaigns);

            if (CollectionUtils.isEmpty(existingCampaigns)) {
                stream = campaigns.stream();
            } else {
                Set<String> registeredCampaignIds = existingCampaigns.stream()
                        .map(MemberCampaign::getCampaignId).collect(Collectors.toSet());
                stream = campaigns.stream().filter(e -> !registeredCampaignIds.contains(e.getId()));
            }
        }

        stream.forEach(e -> {
                memberCampaignRepository.save(new MemberCampaign(member, e.getId()));
            }
        );

    }


}
