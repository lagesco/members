package com.dk.members.service;

import com.dk.campaigns.domain.CampaignResource;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Serviço responsável por obter as campanhas de um determinado time
 *
 * @author Caio Andrade (dk)
 */
@FeignClient(name = "campaignService", url = "http://localhost:8090/api")
public interface CampaignService {

    @RequestMapping("/v1/campaigns/team/{teamId}")
    List<CampaignResource> findByTeamId(@PathVariable("teamId") String cep);

}
