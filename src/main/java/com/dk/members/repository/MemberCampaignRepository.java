package com.dk.members.repository;

import com.dk.members.model.Member;
import com.dk.members.model.MemberCampaign;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * Repositório que realiza a persistencia da associação entre
 * Sócios Torcedores (Member) e Campanha (Campaign) na base de dados
 *
 * @author Caio Andrade (dk)
 */
@Service
public interface MemberCampaignRepository extends MongoRepository<MemberCampaign, String> {

    List<MemberCampaign> findByMember(Member member);

    List<MemberCampaign> findByMemberAndCampaignIdIn(Member member, Set<String> campaignIds);

}
