package com.dk.members.repository;

import com.dk.members.model.Member;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repositório que realiza a persistencia de Sócios Torcedores (Member) na base de dados
 *
 * @author Caio Andrade (dk)
 */
@Repository
public interface MemberRepository extends MongoRepository<Member, String> {

    Member findByEmail(String email);

}
