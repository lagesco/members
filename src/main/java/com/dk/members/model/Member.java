package com.dk.members.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Email;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Entidade representa os dados do documento MongoDB de Member (Membro Torcedor).
 *
 * @author Caio Andrade (dk)
 */
@Document
@ApiModel(value = "Membro")
public class Member implements Serializable {
    @Id
    @ApiModelProperty(value = "Id", notes = "Id da membro", readOnly = true)
    private String id;

    @ApiModelProperty(value = "Nome completo", notes = "Nome completo membro", required = true)
    @Size(min=5, max=100, message="Nome tem capacidade de 5 a 100 caracteres.")
    @NotNull(message="Nome é obrigatório.")
    private String name;

    @Indexed(unique = true)
    @ApiModelProperty(value = "Email", notes = "Email do membro", required = true)
    @Size(min=10, max=150, message="Nome tem capacidade de 10 a 150 caracteres.")
    @NotNull(message="Email é obrigatório.")
    @Email(message = "O e-mail esta com formato inválido.")
    private String email;

    @ApiModelProperty(value = "Data nascimento", required = true)
    @NotNull(message="Data nascimento é obrigatório.")
    private LocalDate birthDate;

    @Indexed
    @Size(min=5, max=100, message="Id do time do coração tem capacidade de 5 a 100 caracteres.")
    @ApiModelProperty(value = "Id time coração", notes = "Id do time do coração", required = true)
    @NotNull(message="Id time do coração é obrigatório.")
    private String teamId;

    public Member() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Member)) return false;
        Member member = (Member) o;
        return Objects.equals(id, member.id) &&
                Objects.equals(name, member.name) &&
                Objects.equals(email, member.email) &&
                Objects.equals(birthDate, member.birthDate) &&
                Objects.equals(teamId, member.teamId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, email, birthDate, teamId);
    }

    @Override
    public String toString() {
        return "Member{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", birthDate=" + birthDate +
                ", teamId='" + teamId + '\'' +
                '}';
    }
}
