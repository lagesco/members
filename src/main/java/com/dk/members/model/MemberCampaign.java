package com.dk.members.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Objects;

/**
 * Associação entre o Sócio Torcedor (Member) e as Campanhas (Campaigns)
 *
 * @author Caio Andrade (dk)
 */
@Document
public class MemberCampaign implements Serializable {
    @Id
    private String id;

    @DBRef
    private Member member;

    @Indexed
    private String campaignId;

    public MemberCampaign() {
        super();
    }

    public MemberCampaign(Member member, String campaignId) {
        this.member = member;
        this.campaignId = campaignId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MemberCampaign)) return false;
        MemberCampaign that = (MemberCampaign) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(member, that.member) &&
                Objects.equals(campaignId, that.campaignId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, member, campaignId);
    }

    @Override
    public String toString() {
        return "MemberCampaign{" +
                "member=" + member +
                ", campaignId='" + campaignId + '\'' +
                '}';
    }
}
