package com.dk.members.controller;

import com.dk.campaigns.domain.CampaignResource;
import com.dk.campaigns.exception.EntityRegisteredException;
import com.dk.members.model.Member;
import com.dk.members.service.CampaignService;
import com.dk.members.service.MemberService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Caio Andrade (dk)
 */
@RestController
@RequestMapping("api/v1/members")
@Api(value = "Sócio Torcedor", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE
        , tags = {"Endpoints Sócio Torcedor"}
        , description = "Requisições da API Rest de Sócio Torcedor")
public class MemberController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MemberController.class);

    private MemberService service;

    private CampaignService campaignService;

    public MemberController(MemberService service, CampaignService campaignService) {
        this.service = service;
        this.campaignService = campaignService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @HystrixCommand(fallbackMethod = "saveFallback", ignoreExceptions = { EntityRegisteredException.class })
    @ApiOperation(value = "Cria uma membro ou retorna as campanhas associadas ao membro, caso contrário retornará as novas campanhas"
            , response = CampaignResource[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success - Usuário cadastrado retorna a lista de campanhas"),
            @ApiResponse(code = 201, message = "Success - Criar um membro"),
            @ApiResponse(code = 204, message = "No Content - Nenhuma campanha a ser retornada"),
            @ApiResponse(code = 400 , message = "Bad Request"),
            @ApiResponse(code = 409 , message = "Conflict"),
            @ApiResponse(code = 500 , message = "Internal Server Error")
    })
    public ResponseEntity<List<CampaignResource>> save(@Valid @RequestBody Member member) {
        LOGGER.debug("Requesting campaings...");
        List<CampaignResource> campaigns = campaignService.findByTeamId(member.getTeamId());
        LOGGER.debug("Got campaigns from service.");

        Member entity = service.findByEmail(member.getEmail());
        if (entity != null) {
            LOGGER.debug("Membro já cadastrado.");
            // salva associação para membro existente
            service.saveCampaigns(entity, campaigns);

            throw new EntityRegisteredException("Membro já cadastrado.");
        }

        if (entity != null && CollectionUtils.isEmpty(campaigns)) {
            return ResponseEntity.noContent().build();
        }

        //salva novo membro
        entity = service.save(member);

        // salva associação para novo membro
        service.saveCampaigns(entity, campaigns);

        return ResponseEntity.status(HttpStatus.CREATED).body(campaigns);
    }

    public ResponseEntity<List<CampaignResource>> saveFallback(Member member) {
        LOGGER.debug("Can't retrieve campaigns, saving user fallback.");

        Member entity = service.findByEmail(member.getEmail());
        if (entity == null) {
            entity = service.save(member);

            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        LOGGER.debug("Membro já cadastrado.");

        throw new EntityRegisteredException("Membro já cadastrado.");
    }
}
