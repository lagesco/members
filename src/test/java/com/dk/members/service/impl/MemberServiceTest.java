package com.dk.members.service.impl;

import com.dk.campaigns.domain.CampaignResource;
import com.dk.members.helper.CampaignGenerator;
import com.dk.members.helper.MemberGenerator;
import com.dk.members.model.Member;
import com.dk.members.model.MemberCampaign;
import com.dk.members.repository.MemberCampaignRepository;
import com.dk.members.repository.MemberRepository;
import com.dk.members.service.MemberService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Testa MemberService
 *
 * @author Caio Andrade (dk)
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MemberServiceTest {

    @Autowired
    private MemberService service;

    @Autowired
    private MemberCampaignRepository memberCampaignRepository;

    @Autowired
    private MemberRepository repository;

    @Before
    public void setup() {
        memberCampaignRepository.deleteAll();
        repository.deleteAll();
        repository.save(MemberGenerator.getAllMembers());
    }

    /**
     * Salva Sócio torcedor e associações de campanhas
     */
    @Test
    public void saveMemberAndMemberCampaignsTest() {
        Member member = MemberGenerator.getMemberToBeSaved();
        Member saved = service.save(member);

        assertThat(saved)
                .as("Deve salvar com sucesso o membro.")
                .isNotNull()
                .extracting("name", "email", "teamId")
                .contains(member.getName(), member.getEmail(), member.getTeamId());

        List<CampaignResource> campaigns = CampaignGenerator.getCampaignsByTeam();
        service.saveCampaigns(saved, campaigns);

        assertThat(memberCampaignRepository.findAll())
                .as("Deve conter 2 campanhas")
                .isNotNull()
                .hasSize(2);
    }

    /**
     * Não deve alterar as campanhas salvas anteiormente
     */
    @Test
    public void saveCampaignsTest() {
        Member member = repository.findAll().get(0);

        // adiciona 2 campanhas ao membro
        List<CampaignResource> campaigns = CampaignGenerator.getCampaignsByTeam();
        campaigns.stream().forEach(c -> {
            memberCampaignRepository.save(new MemberCampaign(member, c.getId()));
        });

        assertThat(memberCampaignRepository.findAll())
                .as("Deve conter somente as duas 2 campanhas")
                .isNotNull()
                .hasSize(2);

        // tenta salvar as mesmas 2 campanhas ao membro, mas não deve ocorrer
        service.saveCampaigns(member, campaigns);

        assertThat(memberCampaignRepository.findAll())
                .as("Deve conter somente as duas 2 campanhas cadastradas anteriormente")
                .isNotNull()
                .hasSize(2);
    }
}
