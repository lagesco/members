package com.dk.members.controller;

import com.dk.campaigns.domain.CampaignResource;
import com.dk.members.helper.CampaignGenerator;
import com.dk.members.helper.MemberBuilder;
import com.dk.members.helper.MemberGenerator;
import com.dk.members.model.Member;
import com.dk.members.service.CampaignService;
import com.dk.members.service.MemberService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Testa o controller de sócio torcedor
 *
 * @author Caio Andrade (dk)
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MemberControllerTest {
    public static final String ROOT_PATH = "/api/v1";

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @MockBean
    private MemberService service;

    @Autowired
    private MemberController controller;

    @MockBean
    private CampaignService campaignService;

    private Member member = MemberGenerator.getAllMembers().get(0);

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();

        Mockito.when(service.findByEmail(member.getEmail())).thenReturn(member);

        List<CampaignResource> campaign = CampaignGenerator.getCampaignsByTeam();

        Mockito.when(campaignService.findByTeamId(campaign.get(0).getTeamId())).thenReturn(campaign);
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.registerModules(new Jdk8Module(), new JavaTimeModule());
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void saveExistingMemberTest() throws Exception {
        System.out.println(asJsonString(member));
        this.mockMvc.perform(post( ROOT_PATH + "/members/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(member)))
                .andDo(print())
                .andExpect(status().isConflict());

        ArgumentCaptor<Member> argumentMember = ArgumentCaptor.forClass(Member.class);
        ArgumentCaptor<List> argumentList = ArgumentCaptor.forClass(List.class);
        verify(service, times(1)).saveCampaigns(argumentMember.capture(), argumentList.capture());
    }

    @Test
    public void saveMemberTest() throws Exception {
        Member member = MemberBuilder.create()
                .withName("Maria Betania")
                .withEmail("mariab@abc.com")
                .withTeamId("TEAM-1")
                .withBirthDate(LocalDate.now())
                .build();

        this.mockMvc.perform(post( ROOT_PATH + "/members/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(member)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$", hasSize(2)));

        ArgumentCaptor<Member> argumentMember = ArgumentCaptor.forClass(Member.class);
        ArgumentCaptor<List> argumentList = ArgumentCaptor.forClass(List.class);
        verify(service, times(1)).saveCampaigns(argumentMember.capture(), argumentList.capture());
    }

    @Test
    public void saveMemberFallbackTest() throws Exception {
        Mockito.when(campaignService.findByTeamId(member.getTeamId())).thenThrow(new RuntimeException());

        Member member = MemberBuilder.create()
                .withName("Maria Betania")
                .withEmail("mariab@abc.com")
                .withTeamId("TEAM-1")
                .withBirthDate(LocalDate.now())
                .build();

        this.mockMvc.perform(post( ROOT_PATH + "/members/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(member)))
                .andDo(print())
                .andExpect(status().isCreated());

        ArgumentCaptor<Member> argumentMember = ArgumentCaptor.forClass(Member.class);
        verify(service, times(1)).save(argumentMember.capture());
    }
}
