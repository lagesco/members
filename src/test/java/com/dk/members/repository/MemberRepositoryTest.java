package com.dk.members.repository;

import com.dk.members.helper.MemberGenerator;
import com.dk.members.model.Member;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Testa repositorio Member
 *
 * @author Caio Andrade (dk)
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MemberRepositoryTest {
    @Autowired
    private MemberRepository repository;

    @Before
    public void setup() {
        repository.deleteAll();
        repository.save(MemberGenerator.getAllMembers());
    }

    @Test
    public void saveTest() {
        Member member = MemberGenerator.getMemberToBeSaved();
        Member saved = repository.save(member);

        assertThat(saved)
                .as("Deve salvar com sucesso o membro.")
                .isNotNull()
                .extracting("name", "email", "teamId")
                .contains(member.getName(), member.getEmail(), member.getTeamId());
    }

    @Test(expected = DuplicateKeyException.class)
    public void saveErrorTest() {
        Member member0 = MemberGenerator.getAllMembers().get(0);

        Member member = MemberGenerator.getMemberToBeSaved();
        member.setEmail(member0.getEmail());

        repository.save(member);
    }

    @Test
    public void findByEmail() {
        Member member = MemberGenerator.getAllMembers().get(0);
        Member entity = repository.findByEmail(member.getEmail());

        assertThat(entity)
                .as("Retorna o primeiro membro(" + member.getName() + ").")
                .isNotNull()
                .extracting("name", "email", "teamId")
                .contains(member.getName(), member.getEmail(), member.getTeamId());
    }
}
