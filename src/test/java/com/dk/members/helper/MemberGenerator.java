package com.dk.members.helper;

import com.dk.members.model.Member;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * @author Caio Andrade (dk)
 */
public class MemberGenerator {

    public static List<Member> getAllMembers() {
        Member member1 = MemberBuilder.create()
                .withName("Josef José")
                .withEmail("josef@abc.com")
                .withTeamId("TEAM-1")
                .withBirthDate(LocalDate.now())
                .build();

        Member member2 = MemberBuilder.create()
                .withName("Maria Cassandra")
                .withEmail("macass@abc.com")
                .withTeamId("TEAM-2")
                .withBirthDate(LocalDate.now())
                .build();

        return Arrays.asList(member1, member2);
    }

    public static Member getMemberToBeSaved() {
        return MemberBuilder.create()
                .withName("Paulo Arroba")
                .withEmail("paulo@abc.com")
                .withTeamId("TEAM-1")
                .withBirthDate(LocalDate.now())
                .build();
    }

}
