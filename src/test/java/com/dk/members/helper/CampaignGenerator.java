package com.dk.members.helper;

import com.dk.campaigns.domain.CampaignResource;
import com.dk.campaigns.helper.CampaignResourceBuilder;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * @author Caio Andrade (dk)
 */
public class CampaignGenerator {

    public static List<CampaignResource> getCampaignsByTeam() {
        return Arrays.asList(
                CampaignResourceBuilder.create()
                        .withId("1a")
                        .withName("Campanha 1")
                        .withTeamId("TEAM-1")
                        .withStartDate(LocalDate.of(2017, 10, 1))
                        .withEndDate(LocalDate.of(2017, 10, 3))
                        .build()

                , CampaignResourceBuilder.create()
                        .withId("2a")
                        .withName("Campanha 4")
                        .withTeamId("TEAM-1")
                        .withStartDate(LocalDate.of(2017, 10, 1))
                        .withEndDate(LocalDate.of(2017, 10, 2))
                        .build()
        );
    }

}
